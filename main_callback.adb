with Gtk.Main;
With Glib; 
With Glib.Main; 
with ada.numerics.discrete_random;

package body Main_Callback is

    function Quit
        (Widget : access Gtk.Window.Gtk_Window_Record'Class)
        return   Boolean
    is
        pragma Unreferenced (Widget);
    begin
        Gtk.Main.Main_Quit;
        return False;
    end Quit;
    
    function Print_Size return boolean 
    is
        use Glib;
        use Gtk.Progress_Bar;
        
        frac : Glib.Gdouble;
    begin
        frac := Glib.Gdouble(Size) / Glib.Gdouble(Size_MAX);
        Set_Fraction(Bar, frac);
        return False;
    end Print_Size;

    task body Produce is
        Source_Id : Glib.Main.G_Source_Id;
        pragma Unreferenced (Source_Id);
        
        type produce_Range is range 1..10;
        package Rand_Int is new ada.numerics.discrete_random(produce_Range);
        use Rand_Int;
        gen : Generator;
        v_rand : produce_Range;
    begin
        reset(gen);
        accept Start;
        while True loop
            delay 0.10;
            v_rand := random(gen);
            if Size + natural(v_rand) > Size_MAX then 
                Size := Size_MAX;
            else    
                Size := Size + natural(v_rand);
            end if;

            exit when Terminate_task;            
            Source_Id := Glib.Main.Idle_Add(Print_Size'access); 
        end loop;
    end Produce;

    procedure Start
    is
    begin
        Produce.Start;
        Consume.Start;
    end Start;
 
    task body Consume is
        Source_Id : Glib.Main.G_Source_Id;
        pragma Unreferenced (Source_Id);
        
        type consume_Range is range 1..200;
        
        package Rand_Int is new ada.numerics.discrete_random(consume_Range);
        use Rand_Int;
        gen : Generator;
        v_rand : consume_Range;
    begin
        reset(gen);
        accept Start;
        while True loop
            delay 2.00;
            
            v_rand := random(gen);
            
            if Size >= natural(v_rand) then 
                Size := Size - natural(v_rand);
            end if;
            
            exit when Terminate_task;
            Source_Id := Glib.Main.Idle_Add(Print_Size'access); 
        end loop;
    end Consume;

end Main_Callback;
