with Gtk.Window;
with Gtk.Handlers;
with Gtk.Progress_Bar;

package Main_Callback is

    package Window_Cb is 
        new Gtk.Handlers.Return_Callback ( Gtk.Window.Gtk_Window_Record,
                                           Boolean);
        
    function Quit(Widget : access Gtk.Window.Gtk_Window_Record'Class)
        return   Boolean;

    Size     : Natural := 1;
    Size_MAX : Natural := 1000;
    
    Bar : Gtk.Progress_Bar.Gtk_Progress_Bar;
        
    Terminate_task : boolean := False;
    
    procedure Start;
    
    task Produce is
        entry Start;
    end Produce;
    
    task Consume is
        entry Start;
    end Consume;
end Main_Callback;
