with Gtk.Box;    use Gtk.Box;
with Gtk.Widget; use Gtk.Widget;
with Gtk.Progress_Bar; use Gtk.Progress_Bar;
with Gtk.Main;
with Gtk.Window; use Gtk.Window;

with Main_Callback; use Main_Callback;

procedure Main is
   Win : Gtk_Window;
   Box : Gtk_Vbox;
begin
    -- Initialize GtkAda, only one thread
    Gtk.Main.Init;

    -- Create a window
    Gtk_New (Win);
    Win.Set_Default_Size (1000, 200);
    
    Window_Cb.Connect(Win,
                     Signal_Delete_Event,
                     Window_Cb.To_Marshaller (Quit'Access));

    Gtk_New_Vbox (Box);
    Win.Add (Box);

    Gtk_New (Bar);
    Set_Fraction (Bar, 0.0);
    Box.Add (Bar);

   -- Show the window
   Win.Show_All;

   -- Start ours task
   Main_Callback.Start;
   
   Gtk.Main.Main;
   Main_Callback.Terminate_task := True;
end Main;
